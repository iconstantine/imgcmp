#-------------------------------------------------
#
# Project created by QtCreator 2015-07-12T02:46:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = imgcmp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    openimagesdialog.cpp

HEADERS  += mainwindow.h \
    openimagesdialog.h

FORMS    += mainwindow.ui \
    openimagesdialog.ui

RESOURCES += \
    appres.qrc
