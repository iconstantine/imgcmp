#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QDialog>
#include <QImage>
#include <QPicture>
#include <QRgb>

#include "openimagesdialog.h"

/// Debug
#include <QDebug>
#define dbg qDebug()

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    openDialog(new OpenImagesDialog)
{
    ui->setupUi(this);
    open = new QAction(QIcon(":/open.png"), tr("&Open"), this);
    ui->mainToolBar->addAction(open);

    connect(open, &QAction::triggered, this, &MainWindow::imageSelect);
    connect(openDialog, &OpenImagesDialog::accepted, this, &MainWindow::compareImages);
    openDialog->setModal(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::imageSelect()
{
    openDialog->show();
    openDialog->getImagePath(left, right);
}

void MainWindow::compareImages() {
    openDialog->getImagePath(left, right);
    QImage img1(left);
    QImage img2(right);


    QImage imgResult (img1.width(), img1.height(), img1.format());

    QRgb red = qRgb(255, 0, 0);
    for (int i = 0; i < img1.width(); i++) {
        for (int j = 0; j < img1.height(); j++) {
            if (img1.pixel(i, j) != img2.pixel(i, j)) {
                imgResult.setPixel(i, j, red);
            } else {
                imgResult.setPixel(i, j, img1.pixel(i, j));
            }
        }
    }

    ui->left->setPixmap(QPixmap::fromImage(img1));
    ui->right->setPixmap(QPixmap::fromImage(imgResult));
//    ui->right->setPixmap(QPixmap::fromImage(img2));

    if (img1 == img2) {
        dbg << "All is OK";
    } else {
        dbg << "Pixmaps are different";
    }
}
