#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QAction;
class OpenImagesDialog;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void imageSelect();
    void compareImages();

private:
    Ui::MainWindow    *ui;
    QAction           *open = nullptr;
    OpenImagesDialog  *openDialog = nullptr;
    QString           left;
    QString           right;
};

#endif // MAINWINDOW_H
