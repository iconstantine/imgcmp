#include "openimagesdialog.h"
#include "ui_openimagesdialog.h"

#include <QFileDialog>

/// Debug
#include <QDebug>
#define dbg qDebug()


OpenImagesDialog::OpenImagesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenImagesDialog),
    fileDialogLeft(new QFileDialog),
    fileDialogRight(new QFileDialog)
{
    ui->setupUi(this);
}

OpenImagesDialog::~OpenImagesDialog()
{
    delete ui;
    delete fileDialogLeft;
    delete fileDialogRight;
}

void OpenImagesDialog::on_selectLeft_clicked() {
    ui->lineEditLeft->setText(fileDialogLeft->getOpenFileName());
}

void OpenImagesDialog::on_selectRight_clicked() {
    ui->lineEditRight->setText(fileDialogRight->getOpenFileName());
}

void OpenImagesDialog::getImagePath(QString& left, QString& right) {
    left = ui->lineEditLeft->text();
    right = ui->lineEditRight->text();
}
