#ifndef OPENIMAGESDIALOG_H
#define OPENIMAGESDIALOG_H

#include <QDialog>

class QFileDialog;

namespace Ui {
class OpenImagesDialog;
}

class OpenImagesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenImagesDialog(QWidget *parent = 0);
    ~OpenImagesDialog();
    void getImagePath (QString& left, QString& right);

private slots:
    void on_selectLeft_clicked();
    void on_selectRight_clicked();

private:
    Ui::OpenImagesDialog *ui;
    QFileDialog          *fileDialogLeft;
    QFileDialog          *fileDialogRight;
};

#endif // OPENIMAGESDIALOG_H
